all: lint test ## Run lint then run all tests

lint: ## Run linters
	black .

test: ## Run all tests with cov
	coverage run --source=. --module pytest --verbose tests && coverage report --show-missing

cov: test ## Run all tests with html coverage
	coverage html && open htmlcov/index.html

help: ## Display this help section
ifeq ($(OSTYPE), linux-gnu)
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z\$$/]+.*:.*?##\s/ {printf "\033[36m%-38s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
else
	@gawk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z\$$/]+.*:.*?##\s/ {printf "\033[36m%-38s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
endif

run: ## Run personal website locally with sqlite
	DB_CONNECTOR=sqlite DB_PATH=$(PWD)/instance/foo.sqlite python app.py

run_with_postgres: ## Run personal website locally with postgres with docker
	docker build -t website:dev .
	docker-compose up

run_with_sqlite: ## Run personal website locally with sqlite with docker
	docker build -t website:dev .
	docker run --rm -p 8080:8080 -e DB_CONNECTOR='sqlite' -v $(PWD)/instance:/app/instance website:dev

clean: ## Clean up the environment
	docker-compose down
	docker rmi website:dev
	rm -rf instance

release: ## Release a new version by specifying with VERSION=
ifndef VERSION
	$(error VERSION is undefined)
endif
ifeq ($(OSTYPE), linux-gnu)
	sed -i '' 's/__version__ = .*/__version__ = "$(VERSION)"/' website/__init__.py
	sed -i '' 's/__version__ == .*/__version__ == "$(VERSION)"/' tests/test_website.py
else
	gsed -i 's/__version__ = .*/__version__ = "$(VERSION)"/' website/__init__.py
	gsed -i 's/__version__ == .*/__version__ == "$(VERSION)"/' tests/test_website.py
endif
	exit 0
	git commit -am "Bump version to $(VERSION)"
	@echo "Tagging version $(VERSION)"
	git tag -a v$(VERSION) -m "v$(VERSION)"
	@echo "Releasing version $(VERSION)"
	git push --tags
	git push
