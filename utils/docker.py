import os


def get_from_file(path: str) -> str:
    """
    Get the content value from the file or return an empty string if the file is not found

    :param key str: The path of the file to search for
    :return str: The value of the key
    """
    value = ""

    try:
        with open(path, "r", encoding="UTF-8") as file:
            value = file.read().strip()
            print(f"{path} has been found in file")
    except FileNotFoundError:
        pass
    return value


def get_config(key: str, default: str = "") -> str:
    """
    Get the value of the key from the environment variables or from a file if it is not defined.
    If the value is not found, return the default value

    :param key str: The key to search for
    :param default str: The default value to return if the key is not found
    :return str: The value of the key
    """
    uppercase_key = key.upper()
    value = os.getenv(uppercase_key)
    if not value:
        print(f"{uppercase_key} has not been found in environment variables")
        path = os.getenv(f"{uppercase_key}_FILE", "")
        value = get_from_file(path)
    if not value:
        print(f"{uppercase_key}_FILE has not been found")
        value = default
    return value


def get_db_info():
    """
    Get the database connection info
    """
    db_info = {"drivername": get_config("db_connector")}
    if db_info.get("drivername", "") == "postgresql":
        db_username = get_config("db_username", default="postgres")
        db_password = get_config("db_password")
        db_host = get_config("db_host")
        db_port = get_config("db_port", default="5432")
        if not (db_username and db_password and db_host and db_port):
            raise RuntimeError("Missing Postgres data")
        db_info["username"] = db_username
        db_info["password"] = db_password
        db_info["host"] = db_host
        db_info["port"] = db_port
    elif db_info.get("drivername", "") == "sqlite":
        if not (db_path := get_config("db_path", default="app.db")):
            raise RuntimeError("Missing SQLITE data")
        db_info["database"] = db_path
    else:
        raise RuntimeError("Missing DB connection info")

    print({i: db_info[i] for i in db_info if i != "db_password"})
    return db_info
