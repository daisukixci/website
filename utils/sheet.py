import pandas as pd


def get_sheet_content(
    sheet_id: str, tab_gid: str = "0"
) -> pd.DataFrame | None:
    csv_url = f"https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv&gid={tab_gid}"
    try:
        return pd.read_csv(csv_url)
    except:
        return None
