#!/usr/bin/env python3

import abc
import random
from typing import Generator, List
import unicodedata
import re

import pydantic


from quiz.question import Question
from korean.words import is_korean


class QuizData(pydantic.BaseModel):
    """
    QuizData describes the mandatory attributes of a quiz
    """

    questions: list[Question] = []
    current_question: int = 0
    score: int = 0
    description: str = ""
    title: str = ""
    url: str = ""
    last_update: str = ""
    enabled: bool = True

    @property
    def total_questions(self) -> int:
        """
        Returns the number of questions
        """
        return len(self.questions)


class AbtrasctQuiz(abc.ABC):
    """
    Required function of a quiz
    """

    def __init__(self, quiz_data: QuizData):
        self.quiz_data = quiz_data

    @abc.abstractmethod
    def _load(self, generator: Generator) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def reset(self) -> None:
        """
        This method should implement a reset of both score and cursor
        """
        raise NotImplementedError

    @abc.abstractmethod
    def check_answers(self, answer: List[str]) -> None:
        """
        This method should implement a verification of the answer
        with an update of the score accordingly
        """
        raise NotImplementedError


def _clean_string(text: str) -> str:
    if is_korean(text):
        return text
    # Normalize the string to remove accents if language is French
    text = (
        unicodedata.normalize("NFKD", text)
        .encode("ASCII", "ignore")
        .decode("utf-8")
    )
    # Remove special characters (keeping only alphanumeric characters and spaces)
    text = re.sub(r"[^a-zA-Z0-9 ]", "", text)
    return text.lower()


class OpenQuestionQuiz(AbtrasctQuiz):
    """
    Implement AbtrasctQuiz to create quiz with a question and open answer
    """

    def __init__(self, data: dict):
        super().__init__(QuizData(**data))

    def _load(self, generator: Generator) -> None:
        self.quiz_data.questions = [
            Question(**question) for question in generator
        ]

    def reset(self) -> None:
        self.quiz_data.current_question = 0
        self.quiz_data.score = 0

    def check_answers(self, answer: List[str]) -> None:
        self.quiz_data.score = 0
        for user_answer, question in zip(answer, self.quiz_data.questions):
            for parsed_answer in self._parse_answer(question.answer):
                if _clean_string(user_answer) == _clean_string(parsed_answer):
                    question.correct = True
                    self.quiz_data.score += 1
                    break

    @staticmethod
    def _parse_answer(answer: str) -> list[str]:
        res = re.sub(r"\(.*\)", "", answer)
        return res.split("/")

    def serialize(self) -> dict:
        """
        Provide a serialization of the quiz data
        """
        return self.quiz_data.model_dump()

    def shuffle_questions(self) -> None:
        """
        Shuffle question order
        """
        random.shuffle(self.quiz_data.questions)

    def shuffle_questions_answers(self) -> None:
        """
        Randomly invert question and answers to ask the question the other way
        """
        for item in self.quiz_data.questions:
            if random.randint(0, 1) == 1:
                previous_question = item.question
                item.question = item.answer
                item.answer = previous_question
