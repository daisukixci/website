import pydantic


class Question(pydantic.BaseModel):
    """
    A question in a quiz.
    """

    question: str
    answer: str
    hint: str | None = None
    correct: bool = False
