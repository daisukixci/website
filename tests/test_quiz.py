from quiz import QuizData, Question
from quiz.quiz import _clean_string


def test_QuizData():
    # GIVEN
    questions = [Question(question="foo?", answer="foo", hint="hint")]
    description = "Lorem Ipsum"
    title = "Foo"
    url = "url_path"
    last_update = "2025/03/01"
    enabled = True

    # WHEN
    quiz_data = QuizData(
        questions=questions,
        description=description,
        title=title,
        url=url,
        last_update=last_update,
        enabled=enabled,
    )

    # THEN
    assert quiz_data.questions == questions
    assert quiz_data.description == description
    assert quiz_data.title == title
    assert quiz_data.url == url
    assert quiz_data.last_update == last_update
    assert quiz_data.enabled == enabled
    assert quiz_data.total_questions == len(questions)


def test__clean_string():
    # GIVEN
    test_strings = [
        "아",
        "nothing to change",
    ]
    expected_strings = [
        "아",
        "nothing to change",
    ]
    # WHEN
    result_strings = []
    for test_string in test_strings:
        result_strings.append(_clean_string(test_string))
    # THEN
    assert result_strings == expected_strings
