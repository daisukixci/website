FROM python:3.10-alpine

EXPOSE 8080
ENV HOME=/home/app
ENV APP_HOME=/home/app/website

RUN apk add --no-cache build-base linux-headers pcre-dev

RUN addgroup --system app && adduser --system app

COPY . $APP_HOME
COPY uwsgi.ini $APP_HOME
RUN chown -R app:app $APP_HOME
WORKDIR $APP_HOME
USER app
ENV PATH="${HOME}/.local/bin/:${PATH}"
RUN python -m pip install --upgrade pip uwsgi
RUN python -m pip install .


CMD ["uwsgi", "--ini", "/home/app/website/uwsgi.ini"]
