#!/usr/bin/env python3

from enum import Enum
from typing import Optional
import pydantic


class Platform(pydantic.BaseModel):
    name: str
    url: str


class MediaType(Enum):
    MOVIE = "movie"
    DRAMA = "series"


class Drama(pydantic.BaseModel):
    title: str = pydantic.Field(validation_alias="Title")
    description: str = pydantic.Field(validation_alias="Description")
    url: Optional[str] = pydantic.Field(validation_alias="URL", default=None)
    platform_name: str = pydantic.Field(validation_alias="Platform")
    country: str = pydantic.Field(validation_alias="Origin")
    media_type: MediaType = pydantic.Field(validation_alias="Type")
    tags_str: str = pydantic.Field(validation_alias="Genre")
    enabled: bool = True
    score: Optional[str] = pydantic.Field(
        default=None, validation_alias="Preference /10"
    )
    vpn_required: bool = pydantic.Field(validation_alias="VPN")

    @property
    def tags(self) -> list[str]:
        return self.tags_str.split(",")

    @property
    def platform(self) -> Platform:
        supported_platforms = {
            "c3po": "https://c3po.home.tuxtrooper.fr/cgi-bin/",
            "AppleTV": "https://tv.apple.com/",
            "Bilibili": "https://www.bilibili.com/",
            "Canal+": "https://www.canalplus.com/",
            "Dailymotion": "https://www.dailymotion.com/",
            "Disney+": "https://www.disneyplus.com/",
            "Netflix": "https://www.netflix.com/",
            "Prime": "https://www.primevideo.com/",
            "TODO": "",
            "Viki": "https://www.viki.com/",
            "Voirdrama": "https://www.Voirdrama.org/",
            "WeTV": "https://wetv.vip/",
            "Youku": "https://www.youku.com/",
            "Youtube": "https://www.youtube.com/",
            "iQiyi": "https://www.iq.com/",
        }
        return Platform(
            name=self.platform_name,
            url=supported_platforms.get(self.platform_name, ""),
        )
