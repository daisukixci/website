overview = """Accustomed to the professional world and flexible. Striving to provide excellence
in my work both in quality and efficiency. Capable of working in a team or independently.
Skilled at multitasking and excellent interpersonal skills."""
achievements = [
    {
        "title": "Endpoint Security Agent development",
        "date": "2023",
        "desc": "Creation of a 3 components Go agent (privileged, unprivileged and UI) communicating using RPC, running on Ubuntu and macOS to ensure security and compliance of the device aligned with the company policy",
    },
    {
        "title": "Browser Extension scoring",
        "date": "2022",
        "desc": "Creation of a scalabale Python AWS lambda which score Chrome Extension risk according to permissions, and it notifies end users when it is above a certain threshold",
    },
    {
        "title": "File analysis container",
        "date": "2021",
        "desc": "Creation of a file analysis container running third parties tools which can be used for manual investigation but also be triggered via a webhook in AWS environment",
    },
    {
        "title": "Talk at ESIEA Secure Edition (ESE)",
        "date": "2017",
        "desc": "Hardening Linux, method and tools",
    },
    {
        "title": "Talk at ESIEA Secure Edition (ESE)",
        "date": "2019",
        "desc": "Use CI/CD for security",
    },
    {
        "title": "Challenge creation for European Cyberweek CTF",
        "date": "2017-2018",
        "desc": "Creation of a hardening challenge which consist of a vulnerable VM which need to be hardened",
    },
]
experiences = [
    {
        "title": "Senior Security Engineer",
        "date": "May 2024 - Now",
        "company": "Datadog",
        "location": "Remote, France",
        "desc": "Ensure Datadog’s security by reducing risks, enhancing features, and providing swift incident responses.\nDevelopment of Datadog features using Go, Python and React to improve Datadog platform users posture within the platform.",
    },
    {
        "title": "Senior IT Security Engineer",
        "date": "May 2022 - May 2024",
        "company": "Datadog",
        "location": "Remote, France",
        "desc": "Development of Go and Python tools that enhance Datadog security posture\nMaintain security of Unix/Linux, macOS and Windows IT managed endpoints\nDevelopment of cloud based and endpoint agent security tools",
    },
    {
        "date": "October 2019 - May 2022",
        "title": "IT Security Engineer",
        "company": "Datadog",
        "location": "Remote, France",
        "desc": "Development of Go and Python tools that enhance Datadog security posture\nMaintain security of Unix/Linux, macOS and Windows IT managed endpoints\nDevelopment of cloud based security tools",
    },
    {
        "date": "July 2015 - October 2019",
        "title": "Cyber defence Engineer",
        "company": "AIRBUS",
        "location": "Élancourt, France",
        "desc": "SOC installation, Hardening of entire Unix/Linux IT infrastructure, remediation at client sites, plug-in programming for cyberdefence solutions\nDevelopment of Java plugins for Graylog SIEM solution",
    },
    {
        "date": "January 2015 - July 2015",
        "title": "Internship",
        "company": "AIRBUS",
        "location": "Élancourt, France",
        "desc": "Programming a tool to generate hardened Linux OS (Gentoo) according to your needs",
    },
    {
        "date": "April 2014 - August 2014",
        "title": "Internship",
        "company": "KMUTT",
        "location": "Bangkok, Thailand",
        "desc": "POC of a forensic tool for network analysis",
    },
]
educations = [
    {
        "date": "2012 - 2015",
        "title": "Engineering",
        "school": "ESIEA",
        "location": "Ivry sur Seine, 94, France",
        "desc": "French engineering school specialized in Information Technology and Electronics offering a five year degree comparable to a Master of Science Specialism in Information Security Data processing, electronic and automatic systems",
    },
    {
        "date": "2009 - 2012",
        "title": "Preparatory class",
        "school": "Parc des Loges",
        "location": "Evry, 91",
        "desc": "Mathematics, Physics and Engineering sciences",
    },
    {
        "date": "2009",
        "title": "Scientific Baccalaureat",
        "school": "Edouard Branly",
        "location": "Créteil, 94",
        "desc": "Specialism in Mathematics and engineering sciences",
    },
]
computer_skills = [
    {
        "category": "Languages",
        "desc": "Python, Golang, React, Bash, Powershell, Java, C",
    },
    {
        "category": "OS",
        "desc": "Linux (Redhat-like, Debian-like, ArchLinux, Gentoo,…), macOS",
    },
    {
        "category": "Cloud environments",
        "desc": "AWS",
    },
    {
        "category": "Endpoint Security",
        "desc": "SentinelOne, AIDE, Samhain, Osquery, Google Workspace",
    },
    {
        "category": "User management security",
        "desc": "Google Workspace, FreeIPA, LDAP, SAML",
    },
    {
        "category": "Email security",
        "desc": "Google Workspace, Armorblox",
    },
    {
        "category": "Network",
        "desc": "Firewall, IDS (Sourcefire, Suricata), Moloch, TAP",
    },
    {
        "category": "Monitoring",
        "desc": "Datadog, Syslog, Graylog, Splunk",
    },
    {
        "category": "VM & Container",
        "desc": "AWS EC2, ESX, Proxmox, Ovirt, Docker Swarm, LXC",
    },
    {
        "category": "Computer Management",
        "desc": "Puppet Enterprise, Jamf, Intune, Foreman, Satellite/SpaceWalk",
    },
    {
        "category": "IaC",
        "desc": "Puppet, Ansible, Terraform, Serverless, Packer",
    },
    {
        "category": "Code & CI",
        "desc": "Git, Gitlab and Gitlab-CI, Github Actions, SonarQube",
    },
]
spoken_languages = [
    {
        "language": "French",
        "level": "Native",
    },
    {
        "language": "English",
        "level": "Fluent - TOEIC score = 825",
    },
]
expertises = [
    "Program in Python, Golang and React on premise and on cloud environments",
    "Deploy, secure and administrate macOS and Linux fleets",
    "Deploy, administrate third party security solutions",
    "Deploy and administrate SOC and monitoring solutions",
    "Use and administrate virtualization and containers plateforms",
    "Analyze, deploy, secure and administrate TCP/IP networks",
    "Teach and mentor junior engineers",
]
