alphabet_quiz = {
    "title": "한글- Alphabet",
    "description": "Provide either the romanization or the Hangul character",
    "url": "korean_alphabet",
    "enabled": True,
    "last_update": "2025-02-12",
    "questions": [
        {
            "question": "ㄱ",
            "answer": "k/g",
        },
        {
            "question": "ㄴ",
            "answer": "n",
        },
        {
            "question": "ㄷ",
            "answer": "t/d",
        },
        {
            "question": "ㄹ",
            "answer": "r/l",
        },
        {
            "question": "ㅁ",
            "answer": "m",
        },
        {
            "question": "ㅂ",
            "answer": "p/b",
        },
        {
            "question": "ㅅ",
            "answer": "s",
        },
        {
            "question": "ㅇ",
            "answer": "ng",
        },
        {
            "question": "ㅈ",
            "answer": "j",
        },
        {
            "question": "ㅊ",
            "answer": "ch",
        },
        {
            "question": "ㅋ",
            "answer": "k",
        },
        {
            "question": "ㅌ",
            "answer": "t",
        },
        {
            "question": "ㅍ",
            "answer": "p",
        },
        {
            "question": "ㅉ",
            "answer": "jj",
        },
        {
            "question": "ㄸ",
            "answer": "tt",
        },
        {
            "question": "ㅃ",
            "answer": "pp",
        },
        {
            "question": "ㅆ",
            "answer": "ss",
        },
        {
            "question": "ㅎ",
            "answer": "h",
        },
        {
            "question": "ㅏ",
            "answer": "a",
        },
        {
            "question": "ㅑ",
            "answer": "ya",
        },
        {
            "question": "ㅓ",
            "answer": "eo",
        },
        {
            "question": "ㅕ",
            "answer": "yeo",
        },
        {
            "question": "ㅗ",
            "answer": "o",
        },
        {
            "question": "ㅛ",
            "answer": "yo",
        },
        {
            "question": "ㅜ",
            "answer": "u",
        },
        {
            "question": "ㅠ",
            "answer": "yu",
        },
        {
            "question": "ㅡ",
            "answer": "eu",
        },
        {
            "question": "ㅣ",
            "answer": "i",
        },
        {
            "question": "ㅐ",
            "answer": "ae",
        },
        {
            "question": "ㅒ",
            "answer": "yae",
        },
        {
            "question": "ㅔ",
            "answer": "ae",
        },
        {
            "question": "ㅖ",
            "answer": "yae",
        },
        {
            "question": "ㅘ",
            "answer": "wa",
        },
        {
            "question": "ㅙ",
            "answer": "wae",
        },
        {
            "question": "ㅚ",
            "answer": "wae",
        },
        {
            "question": "ㅝ",
            "answer": "wo",
        },
        {
            "question": "ㅞ",
            "answer": "wae",
        },
        {
            "question": "ㅟ",
            "answer": "wi",
        },
        {
            "question": "ㅢ",
            "answer": "eui",
        },
    ],
}
