number_quiz = {
    "title": "하나 둘 셋 - Numbers",
    "description": "Fill with the right translation",
    "url": "korean_numbers",
    "enabled": True,
    "last_update": "2025-02-12",
}

sino_korean_numbers_quiz = {
    "title": "일 이 삼 - Sino-Korean Numbers",
    "description": "Fill with the right translation",
    "url": "korean_sino_numbers",
    "enabled": True,
    "last_update": "2025-02-12",
}
