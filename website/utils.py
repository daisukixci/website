"""
Provide utility functions for the application
"""

import string
import random
import re
from typing import List
import pydantic
from utils.sheet import get_sheet_content


from website.models.drama import Drama


def get_random_string(length: int) -> str:
    """
    Generate a random string if it is not defined

    :param length int: Length of the random string to generate
    """
    letters = string.ascii_letters + string.digits + string.punctuation
    return "".join(random.choice(letters) for _ in range(length))


def load_dramas(sheet_id: str, tab_gid: str = "0") -> List[Drama]:
    df = get_sheet_content(sheet_id, tab_gid)
    if df is None:
        return []
    df = df.replace({float("nan"): None})
    dramas = []
    for _, row in df.iterrows():
        try:
            dramas.append(Drama(**row.to_dict()))
        except pydantic.ValidationError:
            print(f"Invalid row: {row}")
    return dramas


def format_question(question: str) -> str:
    """
    Format the question to be more readable

    :param question str: The question to format
    :return str: The formatted question
    """
    suffix = re.search(r"\(.*\)", question)
    question = re.sub(r"\(.*\)", "", question)
    subquestions = question.split("/")
    subquestions = [subquestion.capitalize() for subquestion in subquestions]
    return " ou ".join(subquestions) + (
        f" {suffix.group().capitalize()}" if suffix else ""
    )
