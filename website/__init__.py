#!/usr/bin/env python
"""
Init Flask app
"""

import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.engine.url import URL
from werkzeug.security import generate_password_hash
from flask_login import LoginManager

from website.utils import (
    get_random_string,
    format_question,
)
from utils.docker import get_config, get_db_info

# TODO: make it not hardcoded
__version__ = "0.1.30"


basedir = os.path.abspath(os.path.dirname(__file__))
db_dir = os.path.abspath(os.path.join(basedir, "../.."))

app = Flask(__name__, instance_relative_config=True)
app.secret_key = get_config("secret_key", default=get_random_string(32))
app.jinja_env.globals.update(format_question=format_question)
admin = get_config("admin", default="admin@admin.com")
admin_password = generate_password_hash(
    get_config("admin_password", default="admin"),
    method="scrypt",
)


db_info = get_db_info()
print(f"Connecting to DB {db_info['drivername']}")

db_url = URL.create(**db_info)
app.config["SQLALCHEMY_DATABASE_URI"] = db_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app, engine_options={"pool_pre_ping": True})

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.init_app(app)

from website import auth, views  # noqa: F401,E402
from website.db_models import Boardgame, User  # noqa: F401,E402


@login_manager.user_loader
def load_user(user_id):
    """
    Load user from the database
    """
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return User.query.get(int(user_id))


with app.app_context():
    db.create_all()
    user = User.query.filter_by(
        email=admin
    ).first()  # if this returns a user, then the email already exists in database
    if not user:
        # create a new user with the form data. Hash the password so the plaintext version isn't saved.
        new_user = User(
            email=admin,
            password_hash=admin_password,
        )
        # add the new user to the database
        db.session.add(new_user)
        db.session.commit()
