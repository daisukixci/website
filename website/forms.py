"""
Declare forms
"""

from wtforms import Form, PasswordField, EmailField
from wtforms.validators import DataRequired, Length, Email
from wtforms_sqlalchemy.orm import model_form

from website.db_models.boardgame import Boardgame


class LoginForm(Form):
    """
    Login form
    """

    email = EmailField(
        "Email", validators=[DataRequired(), Length(4, 25), Email()]
    )
    password = PasswordField("Password", validators=[DataRequired()])


BoardgameForm = model_form(Boardgame, exclude=["id", "created_at"])
