from sqlalchemy.sql import func
from sqlalchemy.orm import validates

from website import db


class Boardgame(db.Model):
    """
    Boardgame model which includes the following fields:
    - id: primary key
    - name: name of the boardgame
    - desc: description of the boardgame
    - url: url of the boardgame
    - category: category of the boardgame
    - min_player: minimum number of player
    - max_player: maximum number of player
    - created_at: date of creation
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    desc = db.Column(db.Text)
    url = db.Column(db.String(200))
    category = db.Column(db.String(200))
    min_player = db.Column(
        db.Integer,
    )
    max_player = db.Column(
        db.Integer,
    )
    created_at = db.Column(
        db.DateTime(timezone=True), server_default=func.now()
    )

    def __repr__(self) -> str:
        """
        Define the repr
        """
        return f"<Boardgame {self.name}>"

    @validates("url")
    def validate_url(self, key, url) -> str:
        """
        Validate url
        """
        assert url.startswith("https://"), f"{key} must start with https://"
        return url

    @validates("min_player", "max_player")
    def validate_player(self, key, player) -> int:
        """
        Validate player ensuring min_player is less than max_player and that
        min_player is greater than 0
        """
        assert int(player) >= 1, f"{key} must be greater than 0"
        return player

    @validates("category")
    def validate_category(self, key, category) -> str:
        """
        Validate category ensuring it is one of the supported categories
        """
        supported_categories = [
            "Party game",
            "Initiated",
            "Expert",
            "Family",
            "Strategy",
            "Cooperative",
            "Investigation",
        ]
        assert (
            category in supported_categories
        ), f"{key} must be one of the following: {supported_categories}"
        return category
