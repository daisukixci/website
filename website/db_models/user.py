import re

from sqlalchemy.orm import validates
from flask_login import UserMixin

from website import db


class User(UserMixin, db.Model):
    """
    This class represents the user table
    """

    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password_hash = db.Column(db.String(255), nullable=False)

    @validates("email")
    def validate_email(self, key, email):
        """
        Validate email address
        """
        if not email:
            raise AssertionError("No email provided")
        if not re.match(
            r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email
        ):
            raise AssertionError("Provided email is not an email address")
        return email

    @validates("password_hash")
    def validate_password_hash(self, key, password_hash):
        """
        Validate password_hash
        """
        if not password_hash:
            raise AssertionError("No password provided")
        return password_hash

    def __repr__(self):
        return f"<User {self.name}>"

    def __init__(self, email, password_hash):
        self.email = email
        self.password_hash = password_hash

    def serialize(self):
        return {
            "id": self.id,
            "email": self.email,
        }

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()
