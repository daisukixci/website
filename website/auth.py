#!/usr/bin/env python

from flask import render_template, redirect, url_for, request, flash
from werkzeug.security import check_password_hash
from flask_login import login_user, logout_user, login_required

from website.db_models.user import User
from . import app
from .forms import LoginForm


@app.route("/login", methods=["GET"])
def login() -> str:
    """
    Login page get method to display the login form to the user if they are not logged in already
    """
    return render_template("login.html", page="login", form=LoginForm())


@app.route("/login", methods=["POST"])
def login_post():
    """
    Login page post method to check credentials and log the user in if they exist in the database
    """
    email = request.form.get("email")
    password = request.form.get("password", "")
    remember = bool(request.form.get("remember"))

    user = User.query.filter_by(email=email).first()

    # check if the user actually exists
    # take the user-supplied password, hash it,
    # and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password_hash, password):
        flash("Please check your login details and try again.")
        # if the user doesn't exist or password is wrong, reload the page
        return render_template(
            "login.html", page="login", form=LoginForm(request.form)
        )

    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for("resume"))


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("resume"))
