#!/usr/bin/env python

import os
import pkgutil
from datetime import datetime

from flask import render_template, request, send_from_directory, session
from flask_login import login_required, current_user

from quiz import OpenQuestionQuiz
from korean.numbers import (
    generate_sinokorean_numbers,
    generate_korean_numbers,
)
from korean.words import pick_korean_word, pick_korean_verb
from website.utils import load_dramas
from website.db_models.boardgame import Boardgame
from website.data.resume import (
    overview,
    achievements,
    computer_skills,
    educations,
    experiences,
    spoken_languages,
    expertises,
)
import website.data.quiz.korean as korean_quizzes

from . import app, db
from .forms import BoardgameForm


@app.route("/favicon.ico", methods=["GET"])
def favicon():
    """
    Distribute favicon
    """
    return send_from_directory(
        os.path.join(app.root_path, "static"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


@app.route("/", methods=["GET"])
@app.route("/resume", methods=["GET"])
def resume() -> str:
    """
    Resume page
    """
    return render_template(
        "resume.html",
        page="resume",
        name=current_user.email if current_user.is_authenticated else None,
        overview=overview,
        achievements=achievements,
        experiences=experiences,
        educations=educations,
        computer_skills=computer_skills,
        spoken_languages=spoken_languages,
        expertises=expertises,
    )


@app.route("/howtowork", methods=["GET"])
def howtowork() -> str:
    """
    Resume page
    """
    return render_template(
        "howtowork.html",
        page="howtowork",
        name=current_user.email if current_user.is_authenticated else None,
    )


@app.route("/hobbies/motorcycle", methods=["GET"])
def motorcycle() -> str:
    """
    Motorcycle page
    """
    moto_pictures = [
        os.path.join("/static/moto", i)
        for i in os.listdir(os.path.join(app.root_path, "static/moto"))
    ]
    return render_template(
        "motorcycle.html",
        page="motorcycle",
        name=current_user.email if current_user.is_authenticated else None,
        moto_pictures=moto_pictures,
    )


@app.route("/hobbies/dramas", methods=["GET"])
def dramas() -> str:
    """
    Dramas page
    """
    dramas = load_dramas(
        sheet_id="1Xx0BySKoeKI_YhxAZnJ6eXby7-OlIx4JGINp73Egnqo"
    )
    return render_template(
        "dramas.html",
        page="dramas",
        name=current_user.email if current_user.is_authenticated else None,
        dramas=dramas,
    )


@app.route("/hobbies/boardgames", methods=["GET"])
def boardgames_get() -> str:
    """
    Boardgames page
    """
    boardgames = Boardgame.query.all()
    return render_template(
        "boardgames.html",
        boardgames=boardgames,
        page="boardgames",
        name=current_user.email if current_user.is_authenticated else None,
    )


@app.route("/hobbies/boardgames/create", methods=["GET"])
@login_required
def boardgames_create_form() -> str:
    """
    Boardgames creation page get method
    """
    form = BoardgameForm()

    return render_template(
        "boardgames_create.html",
        form=form,
        page="boardgames_create",
        name=current_user.email if current_user.is_authenticated else None,
    )


@app.route("/hobbies/boardgames/create", methods=["POST"])
@login_required
def boardgames_create_post() -> str:
    """
    Boardgames creation page
    """
    try:
        boardgame = Boardgame(
            name=request.form["name"],
            desc=request.form["desc"],
            url=request.form["url"],
            category=request.form["category"],
            min_player=request.form["min_player"],
            max_player=request.form["max_player"],
            created_at=datetime.now(),
        )
        db.session.add_all([boardgame])
        db.session.commit()
    except AssertionError as e:
        db.session.rollback()
        return render_template(
            "boardgames_create.html",
            form=BoardgameForm(request.form),
            page="boardgames_create",
            error=str(e),
        )
    return boardgames_get()


@app.route("/hobbies/korean", methods=["GET"])
def korean() -> str:
    """
    Korean page
    """
    quizzes = []
    for module in dir(korean_quizzes):
        if not module.startswith("__"):
            for quiz_module in dir(
                pkgutil.resolve_name("website.data.quiz.korean." + module)
            ):
                if not quiz_module.startswith("__"):
                    loaded_quiz = getattr(
                        pkgutil.resolve_name(
                            "website.data.quiz.korean." + module
                        ),
                        quiz_module,
                    )
                    if "enabled" in loaded_quiz and loaded_quiz["enabled"]:
                        quizzes.append(loaded_quiz)

    return render_template(
        "korean.html",
        page="korean",
        name=current_user.email if current_user.is_authenticated else None,
        quizzes=quizzes,
    )


@app.route("/hobbies/korean/vocabulary", methods=["GET"])
def korean_vocabulary() -> str:
    """
    Korean vocabulary page
    """
    korean_vocabulary_quiz = korean_quizzes.vocabulary.vocabulary_quiz
    korean_vocabulary_quiz["questions"] = [
        {"question": question, "answer": answer}
        for question, answer in pick_korean_word(1)
    ]
    saved_quiz = OpenQuestionQuiz(
        data=korean_vocabulary_quiz,
    )
    saved_quiz.shuffle_questions_answers()
    session["korean_vocabulary_quiz"] = saved_quiz.serialize()
    return render_template(
        "quiz.html",
        page="korean_vocabulary",
        name=current_user.email if current_user.is_authenticated else None,
        quiz=saved_quiz,
        back_link="korean",
    )


@app.route("/hobbies/korean/vocabulary", methods=["POST"])
def korean_vocabulary_post() -> str:
    """
    Korean vocabulary page
    """
    user_answers = [value for value in request.form.values()]
    saved_quiz = OpenQuestionQuiz(session.get("korean_vocabulary_quiz", {}))
    saved_quiz.check_answers(user_answers)
    return render_template(
        "quiz_score.html",
        page="korean_vocabulary_post",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
        user_answers=user_answers,
    )


@app.route("/hobbies/korean/conjugation", methods=["GET"])
def korean_conjugation() -> str:
    """
    Korean conjugation page
    """
    korean_conjugation_quiz = korean_quizzes.conjugation.conjugation_quiz
    korean_conjugation_quiz["questions"] = [
        {"question": question, "answer": answer}
        for question, answer in pick_korean_verb(1)
    ]
    saved_quiz = OpenQuestionQuiz(
        data=korean_conjugation_quiz,
    )
    session["korean_conjugation_quiz"] = saved_quiz.serialize()
    return render_template(
        "quiz.html",
        page="korean_conjugation",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
    )


@app.route("/hobbies/korean/conjugation", methods=["POST"])
def korean_conjugation_post() -> str:
    """
    Korean conjugation page
    """
    user_answers = [value for value in request.form.values()]
    saved_quiz = OpenQuestionQuiz(session.get("korean_conjugation_quiz", {}))
    saved_quiz.check_answers(user_answers)
    return render_template(
        "quiz_score.html",
        page="korean_conjugation_post",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
        user_answers=user_answers,
    )


@app.route("/hobbies/korean/numbers", methods=["GET"])
def korean_numbers() -> str:
    """
    Korean numbers page
    """
    korean_numbers_quiz = korean_quizzes.numbers.number_quiz
    korean_numbers_quiz["questions"] = [
        {"question": question, "answer": answer}
        for question, answer in generate_korean_numbers(1)
    ]
    saved_quiz = OpenQuestionQuiz(
        data=korean_numbers_quiz,
    )
    saved_quiz.shuffle_questions_answers()
    session["korean_numbers_quiz"] = saved_quiz.serialize()
    return render_template(
        "quiz.html",
        page="korean_numbers",
        name=current_user.email if current_user.is_authenticated else None,
        quiz=saved_quiz,
        back_link="korean",
    )


@app.route("/hobbies/korean/numbers", methods=["POST"])
def korean_numbers_post() -> str:
    """
    Korean numbers page
    """
    user_answers = [value for value in request.form.values()]
    saved_quiz = OpenQuestionQuiz(session.get("korean_numbers_quiz", {}))
    saved_quiz.check_answers(user_answers)
    return render_template(
        "quiz_score.html",
        page="korean_numbers_post",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
        user_answers=user_answers,
    )


@app.route("/hobbies/korean/sino_numbers", methods=["GET"])
def korean_sino_numbers() -> str:
    """
    Sino-Korean numbers page
    """
    sino_korean_quiz = korean_quizzes.numbers.sino_korean_numbers_quiz
    sino_korean_quiz["questions"] = [
        {"question": question, "answer": answer}
        for question, answer in generate_sinokorean_numbers(1)
    ]
    saved_quiz = OpenQuestionQuiz(
        data=sino_korean_quiz,
    )
    saved_quiz.shuffle_questions_answers()
    session["sino_korean_quiz"] = saved_quiz.serialize()
    return render_template(
        "quiz.html",
        page="korean_sino_numbers",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
    )


@app.route("/hobbies/korean/sino_numbers", methods=["POST"])
def korean_sino_numbers_post() -> str:
    """
    Sino-Korean numbers page
    """
    user_answers = [value for value in request.form.values()]
    saved_quiz = OpenQuestionQuiz(session.get("sino_korean_quiz", {}))
    saved_quiz.check_answers(user_answers)
    return render_template(
        "quiz_score.html",
        page="korean_sino_numbers_post",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
        user_answers=user_answers,
    )


@app.route("/hobbies/korean/alphabet", methods=["GET"])
def korean_alphabet() -> str:
    """
    Korean alphabet page
    """
    alphabet_quiz = OpenQuestionQuiz(
        data=korean_quizzes.alphabet.alphabet_quiz
    )
    session["alphabet_quiz"] = alphabet_quiz.serialize()
    return render_template(
        "quiz.html",
        page="korean_alphabet",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=alphabet_quiz,
    )


@app.route("/hobbies/korean/alphabet", methods=["POST"])
def korean_alphabet_post() -> str:
    """
    Korean alphabet page
    """
    user_answers = list(request.form.values())
    saved_quiz = OpenQuestionQuiz(session.get("alphabet_quiz", {}))
    saved_quiz.check_answers(user_answers)
    return render_template(
        "quiz_score.html",
        page="korean_alphabet_post",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
        quiz=saved_quiz,
        user_answers=user_answers,
    )


@app.route("/hobbies/korean/phrases", methods=["GET"])
def korean_phrases() -> str:
    """
    Korean phrases page
    """
    return render_template(
        "quiz.html",
        page="korean_phrases",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
    )


@app.route("/hobbies/korean/grammar", methods=["GET"])
def korean_grammar() -> str:
    """
    Korean grammar page
    """
    return render_template(
        "quiz.html",
        page="korean_grammar",
        name=current_user.email if current_user.is_authenticated else None,
        back_link="korean",
    )


@app.errorhandler(404)
def page_not_found(error) -> tuple[str, int]:
    """
    Handle 404
    """
    return render_template("404.html", error=error), 404
