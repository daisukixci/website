import random
from utils.sheet import get_sheet_content

_SHEET_ID = "1WUvEYH_hh_zoUkxDkontFYREsR28YnOVmFjYUTSZ0eA"
_VOCABULARY_GID = "663585265"
_VERBS_GID = "445595937"
_korean_vocabulary_sheet = get_sheet_content(
    _SHEET_ID, tab_gid=_VOCABULARY_GID
)
_korean_conjugation_sheet = get_sheet_content(_SHEET_ID, tab_gid=_VERBS_GID)


def is_korean(text: str) -> bool:
    return any("\uac00" <= char <= "\ud7a3" for char in text)


def pick_korean_word(number: int = 1):
    if _korean_vocabulary_sheet is None:
        return
    for _ in range(0, number):
        random_index = random.randint(0, len(_korean_vocabulary_sheet) - 1)
        yield _korean_vocabulary_sheet.iloc[random_index][
            "Hangeul"
        ], _korean_vocabulary_sheet.iloc[random_index]["Traduction"]


def pick_korean_verb(number: int = 1):
    if _korean_conjugation_sheet is None:
        return
    for _ in range(0, number):
        random_index = random.randint(0, len(_korean_conjugation_sheet) - 1)
        yield _korean_conjugation_sheet.iloc[random_index][
            "Verbe"
        ] + " - Infinitif", _korean_conjugation_sheet.iloc[random_index][
            "Infinitif"
        ]
        yield _korean_conjugation_sheet.iloc[random_index][
            "Verbe"
        ] + " - Présent", _korean_conjugation_sheet.iloc[random_index][
            "Présent"
        ]
        yield _korean_conjugation_sheet.iloc[random_index][
            "Verbe"
        ] + " - Impératif", _korean_conjugation_sheet.iloc[random_index][
            "Impératif"
        ]
        yield _korean_conjugation_sheet.iloc[random_index][
            "Verbe"
        ] + " - Passé", _korean_conjugation_sheet.iloc[random_index]["Passé"]
