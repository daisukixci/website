import random

from num2words import num2words


def generate_sinokorean_numbers(limit: int = 10):
    for _ in range(0, limit):
        random_number = random.randint(1, 1000000)
        yield str(random_number), num2words(random_number, lang="ko")


def generate_korean_numbers(limit: int = 10):
    for _ in range(0, limit):
        random_number = random.randint(1, 100)
        yield str(random_number), convert_to_korean(random_number)


def convert_to_korean(number: int) -> str:
    """
    Convert the number to Korean pure numbers

    :param number int: The number to convert
    :return str: The Korean representation of the number
    """
    convert_table = {
        "1": "하나",
        "2": "둘",
        "3": "셋",
        "4": "넷",
        "5": "다섯",
        "6": "여섯",
        "7": "일곱",
        "8": "여덟",
        "9": "아홉",
        "10": "열",
        "20": "스물",
        "30": "서른",
        "40": "마흔",
        "50": "쉰",
        "60": "예순",
        "70": "일흔",
        "80": "여든",
        "90": "아흔",
        "100": "백",
    }
    result = ""
    for position, digit in enumerate(str(number)):
        shift = "0" * int(position - 1)
        if digit == "0":
            continue
        result = convert_table[digit + shift] + result

    return result
