# Website

Personal website code using python, flask and postgresql.

## Usage

To run the website you need to setup a certain number of environment variables explained below. You can either directly pass the value or you can suffix the environment variable with `_FILE` and pass the path to a file containing the value.
For the flask app:

- SECRET_KEY
- ADMIN
- ADMIN_PASSWORD

You can either go with a postgresql database or a sqlite database. You need to set the following environment variables accordingly.
For a postgresql database:

- DB_CONNECTOR
- DB_USERNAME
- DB_PASSWORD
- DB_HOST
- DB_PORT

For a sqlite database:

- DB_CONNECTOR
- DB_PATH

## Development

To setup the development environment, you need to install the dependencies:

```bash
poetry install
```

Make help shows the available commands:

```bash
make help
```

start dev server:

```bash
make run
```

clean:

```bash
make clean
```
